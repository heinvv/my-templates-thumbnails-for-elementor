<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor_Elementor_Library_Thumbnail {

	public function __construct() {

        // add_action('elementor/editor/footer', [ $this, 'template_styling' ], 10, 99 );

        add_filter('manage_elementor_library_posts_columns', [ $this, 'add_img_column' ] );
        add_filter('manage_elementor_library_posts_custom_column', [ $this, 'manage_img_column' ], 10, 2);

	}

    public function add_img_column($columns) {
        $columns['img'] = __( 'Featured Image', 'my-templates-thumbnails-for-elementor' );
        return $columns;
    }

    public function manage_img_column($column_name, $post_id) {
        if( $column_name == 'img' ) {
            echo get_the_post_thumbnail($post_id, 'thumbnail');
        }
        return $column_name;
    }

}