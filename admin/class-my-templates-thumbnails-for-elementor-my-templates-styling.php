<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor_My_Templates_Styling {

	public function __construct() {

        add_action('elementor/editor/footer', [ $this, 'template_styling' ], 10, 99 );

	}

    public function template_styling() {
        ?>
        <script type="text/javascript">        
            <?php
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/js/my-templates-thumbnails-for-elementor-remove-template.js';
            ?>
        </script>
        <style>
            <?php
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/css/my-templates-thumbnails-for-elementor-template.css';
            ?>
        </style>
        <?php
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/my-templates-thumbnails-for-elementor-new-template.php';
    }
}