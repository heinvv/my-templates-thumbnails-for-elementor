<script type="text/template" id="tmpl-elementor-template-library-template-local">
    <div class="elementor-template-library-template-page elementor-template-library-template-remote">
        <div class="elementor-template-library-template-body">
            <# if ( thumbnail ) { #>
                <div class="elementor-template-library-template-screenshot" style="background-image: url({{{ thumbnail }}});"></div>
            <# } else { #>
                <div class="elementor-template-library-template-screenshot" style="background-image: url( '<?php echo plugins_url( '../../assets/img/placeholder.png' , __FILE__ ); ?>' );"></div>
            <# } #>        
            <div class="elementor-template-library-template-preview">
                <i class="eicon-zoom-in-bold" aria-hidden="true"></i>
            </div>
        </div>       
        <div class="elementor-template-library-template-content">
            <div class="elementor-template-library-template-name elementor-template-library-local-column-1">{{{ title }}}</div>
            <div class="elementor-template-library-template-meta elementor-template-library-template-type elementor-template-library-local-column-2">{{{ elementor.translate( type ) }}}</div>
            <div class="elementor-template-library-template-meta elementor-template-library-template-author elementor-template-library-local-column-3">{{{ author }}}</div>
            <div class="elementor-template-library-template-meta elementor-template-library-template-date elementor-template-library-local-column-4">{{{ human_date }}}</div>
            <div class="elementor-template-library-template-controls elementor-template-library-local-column-5">
                <button class="elementor-template-library-template-action elementor-template-library-template-insert elementor-button elementor-button-success">
                    <i class="eicon-file-download" aria-hidden="true"></i>
                    <span class="elementor-button-title"><?php echo __( 'Insert', 'my-templates-thumbnails-for-elementor' ); ?></span>
                </button>
                <div class="elementor-template-library-template-more-toggle">
                    <i class="eicon-ellipsis-h" aria-hidden="true"></i>
                    <span class="elementor-screen-only"><?php echo __( 'More actions', 'my-templates-thumbnails-for-elementor' ); ?></span>
                </div>
                <div class="elementor-template-library-template-more">
                    <div class="elementor-template-library-template-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        <span class="elementor-template-library-template-control-title"><?php echo __( 'Delete', 'my-templates-thumbnails-for-elementor' ); ?></span>
                    </div>
                    <div class="elementor-template-library-template-export">
                        <a href="{{ export_link }}">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        <span class="elementor-template-library-template-control-title"><?php echo __( 'Export', 'my-templates-thumbnails-for-elementor' ); ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>