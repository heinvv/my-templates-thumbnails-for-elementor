<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://abanganimedia.co.za/elementor-my-templates/
 * @since      1.0.0
 *
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/public
 * @author     Abangani Media <info@abanganimedia.co.za>
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $my_templates_thumbnails_for_elementor    The ID of this plugin.
	 */
	private $my_templates_thumbnails_for_elementor;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $my_templates_thumbnails_for_elementor       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $my_templates_thumbnails_for_elementor, $version ) {

		$this->my_templates_thumbnails_for_elementor = $my_templates_thumbnails_for_elementor;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in My_Templates_Thumbnails_For_Elementor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The My_Templates_Thumbnails_For_Elementor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->my_templates_thumbnails_for_elementor, plugin_dir_url( __FILE__ ) . 'css/my-templates-thumbnails-for-elementor-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in My_Templates_Thumbnails_For_Elementor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The My_Templates_Thumbnails_For_Elementor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->my_templates_thumbnails_for_elementor, plugin_dir_url( __FILE__ ) . 'js/my-templates-thumbnails-for-elementor-public.js', array( 'jquery' ), $this->version, false );

	}

}
