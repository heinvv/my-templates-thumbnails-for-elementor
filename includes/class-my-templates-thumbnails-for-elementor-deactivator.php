<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://abanganimedia.co.za/elementor-my-templates/
 * @since      1.0.0
 *
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 * @author     Abangani Media <info@abanganimedia.co.za>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
