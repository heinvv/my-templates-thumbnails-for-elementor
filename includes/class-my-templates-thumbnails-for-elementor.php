<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://abanganimedia.co.za/elementor-my-templates/
 * @since      1.0.0
 *
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 * @author     Abangani Media <info@abanganimedia.co.za>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      My_Templates_Thumbnails_For_Elementor_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $my_templates_thumbnails_for_elementor    The string used to uniquely identify this plugin.
	 */
	protected $my_templates_thumbnails_for_elementor;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'MY_TEMPLATES_THUMBNAILS_FOR_ELEMENTOR_VERSION' ) ) {
			$this->version = MY_TEMPLATES_THUMBNAILS_FOR_ELEMENTOR_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->my_templates_thumbnails_for_elementor = 'my-templates-thumbnails-for-elementor';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->template_styling();
		$this->template_admin_thumbnail();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - My_Templates_Thumbnails_For_Elementor_Loader. Orchestrates the hooks of the plugin.
	 * - My_Templates_Thumbnails_For_Elementor_i18n. Defines internationalization functionality.
	 * - My_Templates_Thumbnails_For_Elementor_Admin. Defines all hooks for the admin area.
	 * - My_Templates_Thumbnails_For_Elementor_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-my-templates-thumbnails-for-elementor-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-my-templates-thumbnails-for-elementor-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-my-templates-thumbnails-for-elementor-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-my-templates-thumbnails-for-elementor-my-templates-styling.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-my-templates-thumbnails-for-elementor-elementor-library-thumbnail.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-my-templates-thumbnails-for-elementor-public.php';

		$this->loader = new My_Templates_Thumbnails_For_Elementor_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the My_Templates_Thumbnails_For_Elementor_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new My_Templates_Thumbnails_For_Elementor_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new My_Templates_Thumbnails_For_Elementor_Admin( $this->get_my_templates_thumbnails_for_elementor(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new My_Templates_Thumbnails_For_Elementor_Public( $this->get_my_templates_thumbnails_for_elementor(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_my_templates_thumbnails_for_elementor() {
		return $this->my_templates_thumbnails_for_elementor;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    My_Templates_Thumbnails_For_Elementor_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	private function template_styling() {

		$template_styling = new My_Templates_Thumbnails_For_Elementor_My_Templates_Styling( $this->get_my_templates_thumbnails_for_elementor(), $this->get_version() );

	}

	private function template_admin_thumbnail() {
			
		$template_admin_thumbnail = new My_Templates_Thumbnails_For_Elementor_Elementor_Library_Thumbnail( $this->get_my_templates_thumbnails_for_elementor(), $this->get_version() );
	
	}

}
