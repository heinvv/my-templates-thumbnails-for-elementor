<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://abanganimedia.co.za/elementor-my-templates/
 * @since      1.0.0
 *
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    My_Templates_Thumbnails_For_Elementor
 * @subpackage My_Templates_Thumbnails_For_Elementor/includes
 * @author     Abangani Media <info@abanganimedia.co.za>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class My_Templates_Thumbnails_For_Elementor_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'my-templates-thumbnails-for-elementor',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
