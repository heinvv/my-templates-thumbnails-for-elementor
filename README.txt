=== Plugin Name ===
Contributors: heinperu
Donate link: https://abanganimedia.co.za/elementor-my-templates/
Tags: elementor, templates
Requires at least: 5.6
Tested up to: 5.8
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds a thumbnail to the My Templates library in Elementor.

== Description ==

This plugin adds a thumbnail to the My Templates library in Elementor both inside the Elementor editor and on the WordPress dashboard.

== Installation ==

Use WordPress' Add New Plugin feature, searching "My Templates Thumbnails for Elementor", or download the archive and:

1. Unzip the archive on your computer
2. Upload `my-templates-thumbnails-for-elementor` directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= The plugin doesn't work, why? =

First, check your version of WordPress: the plugin is not supposed to work on old versions anymore. Make sure also to upgrade to the last version of the plugin!

Then try to deactivate and re-activate it, some user have reported that this fixes some problems.

= What do I need to display the template thumnails? =

You need to make sure that your templates have Featured Images. Once you add Featured Images to the templates, then they should be displayed with thumbail images in the My Templates gallery. 
Besides that you don't have do to do anything. 

= How do I add a Featured Image to my templates? =

On the WordPress Dashboard look for the Templates link and click on the Saved Templates link. Here you can open and edit a saved template. When you edit a Saved Template, you can add a Feautured Image. 

See the screenshots above for more guidance. 

== Screenshots ==



1. The customised My Templates library
2. Featured Image display inside the Saved Templates overview
3. Saved Templates link
4. Featured Image section when editing the Saved Templates.

== Changelog ==

= 1.0 =
* First release *

== Upgrade Notice ==

= 1.0 =
* First release *