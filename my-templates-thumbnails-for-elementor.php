<?php

/**
 *
 * @link              https://abanganimedia.co.za/elementor-my-templates/
 * @since             1.0.0
 * @package    		  My_Templates_Thumbnails_For_Elementor
 *
 * @wordpress-plugin
 * Plugin Name:       My Templates Thumbnails for Elementor
 * Plugin URI:        https://abanganimedia.co.za/elementor-my-templates/
 * Description:       This plugin displays all Elementors templates with a thumbnail inside the the My Templates library and on the WordPress admin pages.
 * adds a thumbnail to the My Templates library in Elementor.
 * Version:           1.0.0
 * Author:            Abangani Media
 * Author URI:        https://abanganimedia.co.za
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       my-templates-thumbnails-for-elementor
 * Domain Path:       /languages
 */

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'MY-TEMPLATES-THUMBNAILS-FOR-ELEMENTOR', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-my-templates-thumbnails-for-elementor-activator.php
 */
function activate_my_templates_thumbnails_for_elementor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-my-templates-thumbnails-for-elementor-activator.php';
	My_Templates_Thumbnails_For_Elementor_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-my-templates-thumbnails-for-elementor-deactivator.php
 */
function deactivate_my_templates_thumbnails_for_elementor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-my-templates-thumbnails-for-elementor-deactivator.php';
	My_Templates_Thumbnails_For_Elementor_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_my_templates_thumbnails_for_elementor' );
register_deactivation_hook( __FILE__, 'deactivate_my_templates_thumbnails_for_elementor' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-my-templates-thumbnails-for-elementor.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

function run_my_templates_thumbnails_for_elementor() {

	$plugin = new My_Templates_Thumbnails_For_Elementor();
	$plugin->run();

}
run_my_templates_thumbnails_for_elementor();
